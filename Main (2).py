import numpy as np
import cv2

img = np.zeros((512, 512, 3), np.uint8)

#circulo verde
cv2.circle(img,(150,300), 80, (0,255,0), -1)
cv2.circle(img,(150,300), 40, (0,0,0), -1)

#triangulo vermelho e verde
pts = np.array([[256, 140],[362, 300],[150,300]], np.int32)
pts = pts.reshape((-1,1,2))
cv2.fillPoly(img, [pts], color=(0,0,0))

#circulo vermelho
cv2.circle(img,(256,140), 80, (0,0,255), -1)
cv2.circle(img,(256,140), 40, (0,0,0), -1)

#triangulo vermelho e verde
pts = np.array([[256, 140],[362, 300],[150,300]], np.int32)
pts = pts.reshape((-1,1,2))
cv2.fillPoly(img, [pts], color=(0,0,0))

#circulo azul
cv2.circle(img,(362,300), 80, (255,0,0), -1)
cv2.circle(img,(362,300), 40, (0,0,0), -1)


#triangulu azul e vermelho
pts2 = np.array([[306, 215],[362, 300],[437, 140]], np.int32)
pts2 = pts2.reshape((-1, 1, 2))
cv2.fillPoly(img, [pts2], color=(0,0,0))

cv2.imshow("teste", img)
cv2.waitKey()
