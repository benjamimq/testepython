from menuTela import Menu
from ManipuladorDeArquivos import ManipulaArq
from AdicionaPessoa import Pessoa
import os
#3import time
#import random



AgendaAbriArq = open("Agenda.txt",'a')

if __name__ == "__main__":

    ManipArq = ManipulaArq()
    NovaPessoa = Pessoa()
    cont = 0
    tela = Menu()
    while True:
        tela.exibir()
        op = input("Selecione uma opção: ")
        if op == '1':
            NovaPessoa.nome = input("Digite o nome do novo contato: ")
            NovaPessoa.email = input("Digite o e-mail do novo contato: ")

            Agenda = open("Agenda.txt", 'r')
            dados2 = Agenda.readlines()
            for verifica in dados2:
                dados = verifica.split(';')
                if dados[1] == NovaPessoa.email:
                    while dados[1] == NovaPessoa.email:
                        print("Este e-mail já está cadastrado.")
                        NovaPessoa.email = input("Digite outro e-mail: ")

            NovaPessoa.tel = input("Digite o numero do novo contato: ")
            cont = cont + 1
            #ManipArq.NovoContato(cont, NovaPessoa.nome, NovaPessoa.email, NovaPessoa.tel)
            ManipArq.NovoContato(cont, NovaPessoa.nome, NovaPessoa.email, NovaPessoa.tel)
            print("Contato adicionado com sucesso!")
            #os.system("pause")
            input()

        elif op == '2':
            print("Contatos:" + '\n', end="")
            #print("Carregando contatos...")
            #time.sleep(random.randint(0,2))
            ManipArq.VerContatos()
            os.system("pause")

        elif op == '3':
            ManipArq.VerContatos()
            Altera = input("Selecione o e-mail do contato que deseja modificar: ")
            cont = cont + 1
            ManipArq.ModificarContato(Altera)
            print("Contato modificado com sucesso!")
            os.system("pause")

        elif op == '4':
            ManipArq.VerContatos()
            n1 = input("Selecione o e-mail do contato que deseja excluir: ")
            cont = cont + 1
            ManipArq.ExcluirContato(n1)
            print("Contato excluido com sucesso!")
            os.system("pause")

        elif op == '5':
            op2 = input("Realmente deseja limpar a agenda? (S/N) ")
            if (op2 == 's' or op2 == 'S'):
                ManipArq.LimpaAgenda()
            print("Agenda limpa com sucesso!")
            #os.system("pause")
            input()

        elif op == '6':
            break
        else:
            print("Opção invalida!")